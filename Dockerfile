FROM python:3.9.16-slim-bullseye
ADD ./app ./app
RUN pip install fastapi
CMD python ./app/__init__.py